/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

var fileReader = new FileReader();
var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

fileReader.onload = function (event) {
	var image = new Image();
	
	image.onload=function(){
		document.getElementById("original-Img").href=image.src;
		document.getElementById("original-Img-resize").src=image.src;
		var imageResizeW = $("#original-Img-resize").width();
		var imageResizeH = $("#original-Img-resize").height();
		var canvas=document.createElement("canvas");
		var context=canvas.getContext("2d");
		canvas.width=imageResizeW;
		canvas.height=imageResizeH;
		context.drawImage(image,
		0,
		0,
		image.width,
		image.height,
		0,
		0,
		canvas.width,
		canvas.height
		);
		document.getElementById("upload-Preview").src = canvas.toDataURL();
		
		var $easyzoom = $('.easyzoom').easyZoom();
		var api1 = $easyzoom.filter('.easyzoom').data('easyZoom');
		if($('.start-training-toolbar').is(':visible')){
			api1.swap(canvas.toDataURL(), image.src);
		}
	}
	image.src=event.target.result;
};

var loadImageFile = function () {
	var uploadImage = document.getElementById("receiptFile");
	var uploadFile = document.getElementById("receiptFile").files[0];
	fileReader.readAsDataURL(uploadFile);
}

;( function( $, window, document, undefined )
{
	$( '.inputfile' ).each( function()
	{
		var $input	 = $( this ),
			$label	 = $input.next( 'label' ),
			labelVal = $label.html();

		$input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
		
		$input.on("change", function () {
			var file = this.files[0];
			var fileType = file["type"];
			var fileSize = this.files[0].size;
			var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
			if (($.inArray(fileType, validImageTypes) < 0) || (fileSize > 4194304)) {
				$("#receiptFile-error").show();
				$(this).parent().addClass("error-parent-class");
				$(".start-training-toolbar").hide();
			}else{
				$("#receiptFile-error").hide();
				$(this).parent().removeClass("error-parent-class");
				$(".input-file-box").addClass("animated fadeOut hidden");
				$(".input-receipt-box").removeClass("hidden").addClass("animated fadeIn");
				$(".change-image").html("<i class='fas fa-receipt'></i> Change");
				$(".start-training-toolbar").removeClass('hidden').show();
				loadImageFile();
			}
		});
	});
})( jQuery, window, document );

