$(document).ready(function() {
	
	if( $('[data-toggle="tooltip"]').length ){
		$('[data-toggle="tooltip"]').tooltip();
	}
	
	$("#login-form").validate({
		rules: {
			username: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			username: {
				required: "Required!"
			},
			password: {
				required: "Required!"
			}
		},
		focusInvalid: false,
		invalidHandler: function(form, validator) {	
			if (!validator.numberOfInvalids())
				return;
			$('html, body').animate({
				scrollTop: $(validator.errorList[0].element).offset().top - 150
			}, 300);
		},
		highlight: function(element) {
			$(element).parent().addClass('error-parent-class');
		},
		unhighlight: function(element) {
			$(element).parent().removeClass('error-parent-class');
		}
	});
	
	$("#change-password-form").validate({
		rules: {
			opassword: {
				required: true
			},
			npassword: {
				required: true,
				validpassword: true
			},
			cpassword: {
				required: true,
				equalTo: "#npassword"
			}
		},
		messages: {
			opassword: {
				required: "Required!"
			},
			npassword: {
				required: "Required!"
			},
			cpassword: {
				required: "Required!",
				equalTo: "Not Match"
			}
		},
		focusInvalid: false,
		invalidHandler: function(form, validator) {	
			if (!validator.numberOfInvalids())
				return;
			$('html, body').animate({
				scrollTop: $(validator.errorList[0].element).offset().top - 150
			}, 300);
		},
		highlight: function(element) {
			$(element).parent().addClass('error-parent-class');
		},
		unhighlight: function(element) {
			$(element).parent().removeClass('error-parent-class');
		}
	});
	$.validator.addMethod("validpassword", function(value, element) {
    return this.optional(element) ||
        /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/.test(value);
	}, "Not valid");
	
});