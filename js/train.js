$(document).ready(function() {
	
	customSelect();
	
	$(".input-receipt-box").mCustomScrollbar({
		theme:"minimal",
		scrollInertia: 300,
		mouseWheel: { preventDefault: true }
	});
	
	$('#testrpt').prop('disabled', true);
	
	if($("#receiptFile").val()!=""){
		$("#receiptFile").change();
	}
	
	
	var receiptFields = {"Invoice Amount":{"elements":{"Region":{"type":"select"},"Search By":{"type":"select"}}},"VAT":{"elements":{"Ignore Previous":{"type":"text"}}},"Date":{"elements":{"Ignore Next":{"type":"text"}}},"Time":{"elements":{"Occurrence":{"type":"text"},"Value Location":{"type":"select"}}},"Discount":{"elements":{"Datatype":{"type":"select"},"Value Length":{"type":"text"}}}};
	
	
	var region_list = '["bottom", "middle", "top"]';
	region_list = JSON.parse(region_list);
	var region_options = '<option value="">Select</option>';
	for (var i = 0; i < region_list.length; i++) {
	   region_options += '<option value="'+region_list[i]+'">'+region_list[i]+'</option>';
	}
	
		
	$("#start-training-form").validate({
		rules: {
			brandName: {
				required: true
			},
			selectMall: {
				required: true
			},
			documentType: {
				required: true
			},
			'featureFields[]': {
				required: true
			},
			receiptFile: {
				required: true,
				extension: "jpg,jpeg,png",
				filesize : 4194304
			}
		},
		messages: {
			brandName: {
				required: "Required!"
			},
			selectMall: {
				required: "Required!"
			},
			documentType: {
				required: "Required!"
			},
			'featureFields[]': {
				required: "Required!"
			},
			receiptFile: {
				required: "Required!",
				extension: "Upload jpg, jpeg, png only"
			}
		},
		focusInvalid: false,
		invalidHandler: function(form, validator) {	
			if (!validator.numberOfInvalids())
				return;
			$('html, body').animate({
				scrollTop: $(validator.errorList[0].element).offset().top - 150
			}, 300);
		},
		highlight: function(element) {
			$(element).parent().addClass('error-parent-class');
		},
		unhighlight: function(element) {
			$(element).parent().removeClass('error-parent-class');
		},
		/*errorPlacement: function(error,element) {
			return true;
		},*/
		submitHandler: function (form) {
			addAnimateCSS(".initial-step-block", "customSlideLeft");
			
			$(".start-training").val("Update Training");
			$('.start-training').prop('disabled', true);
			$(".start-training-toolbar").addClass('hidden');
			$(".update-training-text").removeClass("hidden");
			$(".input-receipt-box").removeClass("hidden").addClass("animated fadeIn");
			$(".input-file-box").removeClass("animated fadeIn").addClass("animated fadeOut hidden");
			
			$(".initial-step-block").on("transitionend webkitTransitionEnd oTransitionEnd", function() {
				$(".training-panel-content").css("height",$(window).height() - 176 +"px");
				$(".feature-fields-content").css("height",$(window).height() - 104 +"px");
				$(".input-file-box").css("height", $(window).height() - 246 +"px");
				$(".input-receipt-box").css("height", $(window).height() - 246 +"px");
			});
			
			$(".feature-fields-block").show();
			
			if(!$(".close-start-training").is(":visible")){
				$(".edit-start-training").show();
			}
			
			$accordion = $('#accordion');
			$contentTopInfo = $('#contentTopInfo');
			$contentBottomInfo = $('#contentBottomInfo');
			var brandName = $("#brandName").val();
			var mallName = $("#selectMall").val();
			var documentType = $("#documentType").val();
			var columnName = $("#featureFields").val();
			var count = $("#featureFields :selected").length;
			var value = null;
			if(count > 0){
				
				$('#accordion').html("");
				$('#contentTopInfo').html("");
				$('#contentBottomInfo').html("");
				$('.accordion-content').removeClass('fadeIn animated').addClass('fadeIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
					$(this).removeClass('fadeIn animated');
				});
				
				var appendHtmlTop = '<div class="brand-receipt-details"><div class="flex-row flex-row-center"><div class="flex-col"><label>Brand Name</label><strong>'+brandName+'</strong></div><div class="flex-col"><label>Mall</label><strong>'+mallName+'</strong></div><div class="flex-col"><label>Document Type</label><strong>'+documentType+'</strong></div><div class="flex-col text-right"><span class="mr-5 text-muted" id="testInfo" data-toggle="tooltip" data-placement="left" title="To Test &amp; Preview the document need to fill all the feature fields"><i class="fas fa-info-circle"></i></span><input type="button" id="testrpt" value="Test & Preview" name="testrpt" class="btn btn-md btn-min-width-80 btn-success" disabled="disabled" /></div></div></div>';
				jQuery(appendHtmlTop).appendTo($contentTopInfo);
				
				var appendHtmlBottom = '<div class="row"><div class="col-xs-6"><div class="mb-15 brand-receipt-details pt-0"><label>Feature Fields</label></div></div><div class="col-xs-6"><div class="text-right form-group"><a href="javascript:void(0)" class="toggle-accordion active" accordion-id="#accordion"></a></div></div></div>';
				jQuery(appendHtmlBottom).appendTo($contentBottomInfo);
				
				var appendHtml = "";
				var columnLength = columnName.length;
				jQuery.each(columnName, function(key, index){
					
					var components = searchKey(index);
					key = key+1;
					
					appendHtml += '<div class="panel panel-default">';
					
					appendHtml += '<div class="panel-heading" role="tab" id="heading'+key+'"><h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse"  href="#collapse'+key+'">'+index+'</a></h4></div>';
					appendHtml += '<div id="collapse'+key+'" class="panel-collapse collapse" role="tabpanel"><div class="panel-body"><div class="row">';
					
					jQuery.each(components, function(comkey, comindex){
						
						appendHtml += '<div class="col-sm-6 col-md-4 col-lg-4"><div class="form-group">';
						appendHtml += '<label>'+comkey+'</label>';
						if(comindex.type=="text"){
							appendHtml += '<input class="form-control form-control-check" id="region'+key+'" name="region'+key+'" />';
						}else{
							appendHtml += '<select class="form-control form-control-check select-single '+key+'_datatype_vals">'+region_options+'</select>';
						}
						appendHtml += '</div></div>';
						
					});
					
					if(columnLength > 1){
						appendHtml += '</div></div><div class="panel-footer"><div class="row"><div class="col-xs-6"><a href="javascript:void(0);" class="btn btn-md btn-min-width-80 btn-grey btn-prev">Prev</a></div><div class="col-xs-6 text-right"><a href="javascript:void(0);" class="btn btn-md btn-min-width-80 btn-primary btn-next">Next</a></div></div></div></div>';
					}else{
						appendHtml += '</div></div>';
					}
					
					appendHtml += '</div>';
					
				});
				
				jQuery(appendHtml).appendTo($accordion);
				
				$('#collapse1').collapse('show');
				
				$('[data-toggle="tooltip"]').tooltip();
				
				$(".close-start-training").trigger("click");
				
				$('#testrpt').prop('disabled', true);
	
			}
			
			customSelect();
			
			return false;
		}
	});
		
	$.validator.addMethod('filesize', function (value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must be less than 4 MB');
    
	$("#contentBottomInfo").on("click", ".toggle-accordion", function() {
		var accordionId = $(this).attr("accordion-id"),
		  numPanelOpen = $(accordionId + ' .collapse.in').length,
		  numPanelTotal = $(accordionId + ' .collapse').length;
		
		$(this).toggleClass("active");
		
		if (numPanelOpen < numPanelTotal) {
		  openAllPanels(accordionId);
		} else {
		  closeAllPanels(accordionId);
		}
	});
	
	openAllPanels = function(aId) {
		$(aId + ' .panel-collapse:not(".in")').collapse('show');
	}
	closeAllPanels = function(aId) {
		$(aId + ' .panel-collapse.in').collapse('hide');
	}
	
	
	$('#accordion').on("click",".btn-next",function(){
		var _is_error = false;
		$(this).closest('.panel').find(".form-control-check").each(function(){
			if($(this).val() == ""){
				$(this).parent().addClass("error-parent-class");
				_is_error = true;
			}else{
				$(this).parent().removeClass("error-parent-class");
			}
		});
		if(_is_error){
			$(this).closest('.panel').addClass("error-panel-class");
			return false;
		}else{
			$(this).closest('.panel').removeClass("error-panel-class");
			$(this).closest('.panel').find(".panel-collapse").collapse('hide');
			$(this).closest('.panel').next().find(".panel-collapse").collapse('show');
		}
	});
	
	$('#accordion').on("click",".btn-prev",function(){
		$(this).closest('.panel').find(".panel-collapse").collapse('hide');
		$(this).closest('.panel').prev().find(".panel-collapse").collapse('show');
	});

	$('#accordion').on('hide.bs.collapse', function (e) {
		var _is_error = false;
		
		$(e.target).closest('.panel').find(".form-control-check").each(function(){
			if($(this).val() == ""){
				$(this).parent().addClass("error-parent-class");
				_is_error = true;
			}else{
				$(this).parent().removeClass("error-parent-class");
			}
		});
		
		if(_is_error){
			$(e.target).closest('.panel').addClass("error-panel-class");
		}else{
			$(e.target).closest('.panel').removeClass("error-panel-class");
		}
	});	
	
	$(".edit-start-training").click(function(e){
    	addAnimateCSS(".initial-step-block", "customSlideRight");
		$(this).hide();
		$(".close-start-training").show();
		$(".start-training-toolbar").removeClass('hidden').show();
    });
	
	$(".close-start-training").click(function(e){
    	removeAnimateCSS(".initial-step-block", "customSlideRight");
		$(this).hide();
		$(".edit-start-training").show();
		$(".start-training-toolbar").hide();
		$(".input-receipt-box").removeClass("hidden").addClass("animated fadeIn");
		$(".input-file-box").removeClass("animated fadeIn").addClass("animated fadeOut hidden");
		$(".change-image").html("<i class='fas fa-receipt'></i> Change");
		$('.close-start-training').prop('disabled', false);
		$(".initial-step-block").removeClass("no-allow")
    });
	
	$(".change-image").click(function(e){
		if($(".input-file-box").is(":visible")){
    		$(".input-receipt-box").removeClass("hidden").addClass("animated fadeIn");
			$(".input-file-box").removeClass("animated fadeIn").addClass("animated fadeOut hidden");
			$(this).html("<i class='fas fa-receipt'></i> Change");
		}else{
			$(".input-receipt-box").removeClass("animated fadeIn").addClass("hidden");
			$(".input-file-box").removeClass("animated fadeOut hidden").addClass("animated fadeIn");
			$(this).html("<i class='fas fa-receipt'></i> Show");
		}
    });
	
	$('.zoom-toggle').click(function(){
		if($(this).html() == '<i class="fas fa-search-plus"></i> Zoom Off'){
		   $(this).html('<i class="fas fa-search-plus"></i> Zoom On');
		   $(".input-receipt-box").find(".easyzoom").addClass("no-allow");
		}
		else{
		   $(this).html('<i class="fas fa-search-plus"></i> Zoom Off');
		   $(".input-receipt-box").find(".easyzoom").removeClass("no-allow");
		}
    });
	
	$('.feature-fields-content').on("click","#testrpt",function(){
		$('#testModal').modal('show');
		$('.modal-backdrop').addClass('custom-modal-backdrop');
		var testArray = [];
		
		testArray.push({'docDetails':{'Brand Name': $("#brandName").val(),'Mall': $("#selectMall").val(),'Document Type': $("#documentType").val()}});
		
		
		//$("#testModal").find(".modal-body").html(JSON.stringify(testArray));
		
		$("#testModal").find(".test-back-content").html("<div class='flex-row'><div class='flex-col'><div class='form-group'><label>Brand Name</label><p>Zara</p></div><div class='form-group'><label>Mall</label><p>Deira City Center</p></div><div class='form-group'><label>Document Type</label><p>Receipt</p></div><div class='form-group'><label>Feature Fields</label><p>Invoice Amount <span>Region: bottom, Search by: bottom</span></p><p>Total <span>Region: bottom</span></p></div></div><div class='flex-col flex-col-310 tbc-doc pos-rel'><img src='images/receipt.jpg'/><span style='top:557px;left:51px;width: 44px;height: 21px;'>Total</span></div></div>");
	});
	
	$(".feature-fields-overlay").click(function(e){
		if(!$(".initial-step-block").hasClass("no-allow")){
			$(".close-start-training").trigger("click");
		}
	});
	
	function searchKey(key) {
		var value = null;
		jQuery.each(receiptFields, function(k, index){
			if(k==key){
				value = index.elements;
			}	
		})
		return value;
	}
	
	function customSelect(){
		$(".select-single").select2();
		
		$(".select-multiple").select2();
		
		$(".select-single, .select-multiple").each(function(){
			$(this).on("change", function () {
				if(this.value != ""){
					$(this).parent().removeClass("error-parent-class");
				}else{
					$(this).parent().addClass("error-parent-class");
				}
			});
		});
		
		$(".form-control").each(function(){
			$(this).on("keyup", function () {
				if(this.value != ""){
					$(this).parent().removeClass("error-parent-class");
				}else{
					$(this).parent().addClass("error-parent-class");
				}
			});
		});
		
		$('.form-control-check').bind("keyup change",function() {
			var empty = false;
			$('.form-control-check').each(function() {
				if ($(this).val().length == 0) {
					empty = true;
				}
			});
			if (empty) {
				$('#testrpt').prop('disabled', true);
				$('#testInfo').show();
				removeAnimateCSS("#testrpt", "shake");
			} else {
				$('#testrpt').prop('disabled', false);
				$('#testInfo').hide();
				addAnimateCSS("#testrpt", "shake");
			}
		});
		
		$("#start-training-form").dirrty().on("dirty", function(){
			$('.start-training').prop('disabled', false);
			$('.close-start-training').prop('disabled', true);
			$('.initial-step-block').addClass('no-allow');
		}).on("clean", function(){
			$('.start-training').prop('disabled', true);
			$('.close-start-training').prop('disabled', false);
			$('.initial-step-block').removeClass('no-allow');
		});
	}
	
	function addAnimateCSS(element, animationName, callback) {
		const node = document.querySelector(element)
		node.classList.add("animated", animationName)
	}
	
	function removeAnimateCSS(element, animationName, callback) {
		const node = document.querySelector(element)
		node.classList.remove("animated", animationName)
	}
	
	
	$("#tpc-empty").addClass("animated fadeOut hidden");
	$("#tpc-data").removeClass("hidden").addClass("animated fadeIn");
	
});